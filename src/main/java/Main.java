import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Main {

    private static final int newWidth = 300;

    public static void main(String[] args) {
        String srcFolder = "C:\\Users\\User\\Desktop\\testSize";
        String dstFolder = "C:\\Users\\User\\Desktop\\resize";

        File srcDir = new File(srcFolder);

        long start = System.currentTimeMillis();

        File[] files = srcDir.listFiles();

        int core = Runtime.getRuntime().availableProcessors();
        List<File[]> allFiles = new ArrayList<>();

        for (int i = 0; i < core; i++) {
            File[] files1 = new File[(i == core - 1) ? Objects.requireNonNull(files).length - files.length / core * core + files.length / core
                    : Objects.requireNonNull(files).length / core];
            System.arraycopy(files, i * (files.length / core), files1, 0, files1.length);
            allFiles.add(files1);
        }
        for (File[] allFile : allFiles) {
            System.out.println(allFile.length);
            ImageResizer resizer = new ImageResizer(files, newWidth, dstFolder, start);
            resizer.start();
        }
    }
}
